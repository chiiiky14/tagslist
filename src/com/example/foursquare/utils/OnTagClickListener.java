package com.example.foursquare.utils;

/**
 * listener for tag delete
 */
public interface OnTagClickListener {
	void onTagClick(Tag tag, int position);
}