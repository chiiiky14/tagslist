package com.example.foursquare.utils;

/**
 * listener for tag delete
 */
public interface OnTagDeleteListener {
	void onTagDeleted(Tag tag, int position);
}